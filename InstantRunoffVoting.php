<?php

class InstantRunoffVoting
{
    private $candidates;
    private $ballots;
    private $result;
    private $percentage;
    public $winner;

    public function __construct ($candidates, $ballots)
    {
        echo __CLASS__ . "\n\n";

        $this->candidates = $this->startIndexFrom1(explode(PHP_EOL, $candidates));

        $ballots = explode(PHP_EOL, $ballots);
        $votes = [];
        foreach ($ballots as $key => $value) {
            $votes[$key] = $this->startIndexFrom1(array_map('trim', explode(',', $value)));
        }
        $this->ballots = $this->startIndexFrom1($votes);

        $this->winner = null;
    }

    public function randomCandidatesNumber ($candidatesNumber)
    {
        $inputString = '123456789abcdefghijklmnopqrstuvwxyz';
        if ($candidatesNumber > strlen($inputString)) {
            $candidatesNumber = strlen($inputString);
        }
        return implode(',',
            array_map(function ($char) {
                if (0 === intval($char)) {
                    return ord($char) - 87;
                }
                return intval($char);
            },
                str_split(
                    str_shuffle(
                        substr($inputString, 0, $candidatesNumber)
                    )
                )
            )
        );
    }

    private function calcPercentage ()
    {
        $sum = array_sum($this->result);

        foreach ($this->result as $key => $value) {
            $this->percentage[$key] = round($value * 100 / $sum, 2, PHP_ROUND_HALF_DOWN);
        }
    }

    private function startIndexFrom1 ($array)
    {
        return array_combine(range(1, count($array)), array_values($array));
    }

    public function printCandidates ()
    {
        echo "Candidates: \n";
        foreach ($this->candidates as $position => $name) {
            echo $position . ' - ' . $name . "\n";
        }
        echo "\n";
    }

    public function printBallots ($round)
    {
        echo "Ballots of round $round: \n";
        foreach ($this->ballots as $ballot) {
            foreach ($ballot as $option) {
                echo $option . " ";
            }
            echo "\n";
        }
        echo "\n";
    }

    public function printWinner ()
    {
        if (empty($this->winner)) {
            echo "There is no winner!";
        } else {
            echo "The winner is: {$this->candidates[$this->winner]}\n";
        }
    }

    private function removeOptionFromBallots ($remove)
    {
        echo "Remove the loser candidate: {$this->candidates[$remove]}\n";
        foreach ($this->ballots as $key => $ballot) {
            foreach ($ballot as $position => $option) {
                if ($remove == $option) {
                    unset($this->ballots[$key][$position]);
                    unset($this->percentage[$option]);
                    unset($this->result[$option]);
                }
            }
            $this->ballots[$key] = $this->startIndexFrom1($this->ballots[$key]);
        }
    }

    private function count ()
    {
        $this->result = [];

        foreach ($this->ballots as $ballot) {
            foreach ($ballot as $position => $option) {
                if (1 === $position) {
                    if (empty($this->result[$option])) {
                        $this->result[$option] = 1;
                    } else {
                        $this->result[$option] = $this->result[$option] + 1;
                    }
                    arsort($this->result);
                }
            }
        }

        $this->calcPercentage();
    }

    private function printResult ($round)
    {
        echo "The result of round $round (candidate->votes): ";
        foreach ($this->result as $key => $value) {
            echo "$key->$value ";
        }

        echo "\n\n";
    }

    private function findWinner ()
    {
        foreach ($this->percentage as $candidate => $percent) {
            if ($percent > 50) {
                $this->winner = $candidate;
            }
        }
    }

    private function removeLooser ()
    {
        $minCandidate = null;
        $minPercentage = 100;
        foreach ($this->percentage as $candidate => $percent) {
            if ($percent < $minPercentage) {
                $minCandidate = $candidate;
                $minPercentage = $percent;
            }
        }

        if (!empty($minCandidate)) {
            $this->removeOptionFromBallots($minCandidate);
        }
    }

    private function round ($no)
    {
        $this->printBallots($no);
        $this->count();
        $this->removeLooser();
        $this->findWinner();
        $this->printResult($no);
    }

    public function process ()
    {
        $rounds = 1;
        $roundLandmark = $this->ballots;
        if (count($this->ballots) < count($this->candidates)) {
            $roundLandmark = $this->candidates;
        }
        while (empty($this->winner) && $rounds <= count($roundLandmark)) {
            $this->round($rounds);
            $rounds++;
        }
        echo "\n";
    }
}

$candidates = <<<EOT
(1).one
(2).two
(3).three
(4).four
(5).five
(6).six
EOT;

$ballots = <<<EOT
2,3,5,1,4,6
5,3,1,2,4,6
6,4,1,2,3,5
2,1,3,4,5,6
3,2,4,5,1,6
EOT;

$irv = new InstantRunoffVoting($candidates, $ballots);
$irv->printCandidates();
$irv->process();
$irv->printWinner();
